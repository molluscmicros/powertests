Power tests for wifi whelk boards
=================================

A range of power tests for the F103 and L073 versions of the board.
Indentical code versions are compiled for each target, they are loaded onto a board then swapped between the test circuit.
They are owered via USB and a regulator on a simple squid board, with a multimeter used for measuring the current.

Microcontrollers
----------------

Two WifiWhelk boards were used:

* STM32F103RB running at 72 MHz (external 8 MHz oscillator, scaled by 9)
* STM32L073RZ running at 32 MHz (external 8 MHz oscillator, scaled by 4)
 
Tests
-----

1. Polling button with wait: 56479bbb59
2. Interrupt button with sleep(): a049ea5de03
3. Interrupt button with __WFI(): fe62c4053
4. Interrupt button with sleep() followed by lots of division: aa1d94866a7e
    * For this test each time a button is pressed a buch of maths operations are performed
    * The time required for the operations is measured and printed
    * In the current results the first value gives current and duration of calculation, the second gives idle current
5. Interrupt button with rtos: 
6. Interrupt button with deepsleep(): 43cfb2ea33da

7. Interrupt with rtos and __WFI: 550d2e717
8. Interrupt with rtos and deepsleep: 4a116d8
9. Ticker with rtos and __WFI: 4c7e1d4bb8c
10. Ticker with rtos and sleep: ab6f2886c
11. Ticker with rtos and deepsleep: 98c044dbaf
    * This doesnt work since the deepsleep stops the ticker.

Results
-------


| Test | F103 current (mA) | L073 current (mA) |
|------|-------------------|-------------------|
| 1    | 27.8              | 7.8               |
| 2    | 10.9              | 4.3               |
| 3    | 10.9              | 4.1               |
| 4 calc | 33.3(6.1 s)     | 9.4 (33.6 s)      |
| 4 idle | 11.4            | 4.5               |
| 5    | 27.5              | 7.8               |
| 6    | 1.5               | 0.53              |
| 7    |                   | 4.0               |
| 8    |                   | 0.06              |
| 9    |                   | 3.8               |
| 10   |                   | 3.8               |
| 11   |                   | N/A               |

Interpretation
--------------

The L073 runs at something like 33 % of the F103 current.
However, it takes more time to do some tasks (notably division?).
In the 4th test this means it used about 55% extra charge, despite the lower current, due to the increase computation time.
With deepsleep they could run for a while from a coin cell (O(100 mAhr) -> O(days)).

According to the chip specs I should be able to drop into O(uA) when in stop mode. 
I suspect the mbed HAL is waking them more frequently for its tickers, etc.
I should see if I can really suspend those to get a long term usage from battery power.

The mbed rtos runs like a wait, not sleep.
To get the rtos low power a `Thread::attach_idle_hook()` call is needed to put into a low power mode.
With that enabled and deepsleep I reach ~60 uA, which could run on a battery for a month or so.
With deepsleep I reply on an external interrupt to wake up, I should work out how to have the RTC do a wake up.


Due to the different clock speeds the F103 should run at 72 / 32 = 2.25 more current than the L073.
This would be 44 % of the clock speed.
I see more like 33 %, so the lower power chip is outperforming the F103 in this sense, but perhaps I should redo some tests with the F103 clock slowed to 32 MHz to check.
