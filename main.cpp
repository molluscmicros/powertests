#include "mbed.h"

//InterruptIn button(p8);
DigitalOut led1(LED1);
Thread * counter = NULL;

static void idlehook(void) {
    deepsleep();
}

static void press() {
    led1 = !led1;
    if (counter != NULL) {
        counter->signal_set(1);
    }
}

void ledcontrol(void) {
        int count = 0;
        DigitalOut led(LED0);
        led = 0;
        //button.rise(&press);
        Ticker t;
        t.attach(&press, 10.0);
        while (true) {
            Thread::signal_wait(1);
            count++;
            led = (count % 5 == 0);
        }
}

int main() {
    Thread::attach_idle_hook(idlehook);
    //ser.baud(57600);
    led1 = 0;
    counter = new Thread();
    counter->start(ledcontrol);
}
